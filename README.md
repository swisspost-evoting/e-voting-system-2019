# Disclosure E-Voting-System 2019 

In 2019, Swiss Post has disclosed the source code of its old e-voting solution. This relates to the implementation of the cryptographic protocol for full verifiability at the application level. The specification of the cryptographic protocol and the architecture of the e-voting solution were published.

A Gitlab.com account and registration with swisspost is required to view the old source code. The old source code has been archived and is still accessible to all existing users.

If you have already registered in the past and had access to the source code, you can access it via [the following link](https://gitlab.com/swisspost/evoting-solution)
If you are interested in the old, archived source code, please send us an email to evoting@post.ch, including your Gitlab username.
