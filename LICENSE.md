**Electronic Voting Solution Source Code Access Agreement**

1.	Summary

The aim of publishing the source code is to establish and build confidence in the public domain, while obtaining feedback from professional experts and the opportunity to make improvements. Constructive contributions and findings on the source code are always welcome.
At this stage, we would like to highlight the key points in the terms of use:
* The OEV (Federal Chancellery Ordinance on Electronic Voting) allows the source code to be examined, modified, compiled and executed for ideational purposes. Publication is organized and regulated in line with this idea.
* The published code, including the source code, is the intellectual property of the companies Scytl and Post CH Ltd. The code in question is proprietary and not subject to a free and open source software (FOSS) licence.
* Distributing or publishing the access tools, granting access to third parties and sharing the source code are not permitted.
* Post CH Ltd must be consulted before any findings may be published. 
* All feedback regarding vulnerabilities found in the electronic voting solution source code is welcome. All feedback must be reported on the GitLab.com platform as an issue and set to “confidential”. Swiss Post will incorporate the reported vulnerabilities into the development of the system.

2.	INTRODUCTION

This Electronic Voting (EV) Solution Source Code Access Agreement (“the Agreement”) governs the access to the EV Solution Source Code (“the EV Solution Source Code” or “the Source Code”), consisting of (a) the source code of the EV Solution cryptographic protocols and/or its security measures and any other components of the EV Solution as well as (b) the documentation on the system and its operation, as made accessible by its Owners in this Source Code Access Program (“the Program”). The Program includes release 2.1 of the Source Code. The documents and software components made public under www.swisspost.ch/evoting are not part of the Program.
Scytl Secure Electronic Voting, S.A. (Scytl) and Post CH Ltd (Swiss Post) are the Owners of the EV Solution and the Source Code.
The Owners own and retain all rights, titles and interests in and to their respective parts of the EV Solution Source Code, including all copyright, patents, trade secret rights, trademarks, and other intellectual property rights therein. 
The Owners grant access to the EV Solution Source Code in the Program to the extent required by the (Swiss) Federal Chancellery Ordinance on Electronic Voting (“the Ordinance”) . No part of this Agreement shall be construed as to provide surpassing rights or to permit its use for other purposes. 
The Program takes place because the system has the property of complete verifiability and the system and its operation have been successfully tested according to the Ordinance. The Program aims to give interested persons sufficient time to analyse the Source Code and submit their results to the authorities and to the system providers prior to the canton’s application for general approval. 
The Owners welcome independent security researchers (Researchers) to evaluate and report any vulnerabilities that may be found in the EV Solution Source Code.

3.	AGREEMENT TO THE EV SOLUTION SOURCE CODE ACCESS AGREEMENT

By signing up to gain access to the EV Solution Source Code, you agree to be bound by the Agreement for the duration of the access to the EV Solution Source Code and thereafter. 

4.	ORGANIZATION OF THE PROGRAM

The Program begins on 07.02.2019 and ends at the termination of the productive use of the release 2.1. 
Swiss Post provides a repository dedicated to the Program on the GitLab platform (https://www.gitlab.com/swisspost). Researchers will receive access to the repository with their GitLab.com account. The GitLab account is free of charge. Access to the repository is limited for the duration of the Program. Accesses to the repository will be logged. 
Participants who have found or believe they have found a vulnerability are obliged to submit a report in the GitLab platform as an issue set explicitly to confidential (see para. 7 FAIR USE RESTRICTIONS). The publication of any vulnerability or other test findings has to comply with the Disclosure Policy (see para. 8 REPORTING PROCEDURE), which fully replaces the Responsible Disclosure Policy of GitLab under the Program.
The Owners will make their best efforts to give timely feedback on a submitted finding. 

5.	REGISTRATION POLICY

Registration is mandatory to access the EV Solution Source Code. 
You can register via http://www.swisspost.ch/evoting-sourcecode (website operated by Swiss Post). If participating as a team, all members need to register separately. 
Registration will allow you to access the GitLab repository dedicated to the Program and use the EV Solution Source Code within the scope of the Regulatory License (see para. 6 REGULATORY LICENSE). The means of access are personal and may not be shared with anyone.
Registration for the Program is open to all natural persons willing to comply with the Agreement, with the exception of natural persons who do not act under their own responsibility, but as employees, civil servants, officers or any other subordinate capacity. Registration is therefore not open to organizations, associations, institutions, administrations, governments, government agencies, foreign states, or any other entity that is not a natural person.
The registration data will only be used by Swiss Post to ensure the proper operation of the Program and compliance with this Agreement. 

6.	REGULATORY LICENSE

The EV Solution Source Code may be examined, modified, compiled and executed for ideational purposes and Researchers may write and publish studies thereon (“the Purpose”); any other use of the EV Solution Source Code is forbidden, unless a legally valid license is granted by the Owners. 
Unauthorized reproduction, distribution, transformation, public communication of the EV Solution outside the Purpose is illegal, and may result in civil or criminal liability. 

7.	FAIR USE RESTRICTIONS

The EV Solution Source Code can be accessed for the Purpose under the following fair use rules:
* Researchers cannot use the EV Solution Source Code for any kind of commercial purposes, including but not limited to, the creation of a product, derivative works of the EV Solution and/or the provision of services.
* It is forbidden to remove or alter any notices regarding intellectual property rights such as copyright, patents, trademarks or other proprietary rights. 
* Researchers shall follow the Reporting Procedure to communicate any vulnerabilities, bugs, errors or other issues (“the Vulnerabilities”) found in the EV Solution Source Code and comply with the Responsible Disclosure rules.
* Researchers shall conduct tests following the deployment, configuration, trust assumptions and infrastructure specifications also published by the Owners. 
* Researchers shall comply with the applicable law and shall not breach any agreements in order to find Vulnerabilities.
* Researchers shall not exploit any found Vulnerability on any real elections or votes, or perform any action that is intended to negatively affect the reputation of any of the Owners, any election or vote, electoral authority and any election technology providers and partners.
* Researchers shall avoid any attempt to modify or destroy data
.

8.	REPORTING PROCEDURE

Any Vulnerability found in the EV Solution Source Code must be reported in the communication channels provided by the Owners. When communicating via the GitLab platform, Researchers are obliged to submit a report as an issue set explicitly to confidential, and thus make it visible only to team members.

Researchers shall provide sufficient information to reproduce the Vulnerability so that the Owners can act as quickly as possible. Usually, a vulnerability description is sufficient, but for more complex vulnerabilities, more detailed information may be needed.

The Researcher e-mail address must not be managed or accessible by more than one person and the information received in the Researcher e-mail account must not be shared with or forwarded to any other e-mail account.

The Owners will acknowledge reception of the report, and will decide, at their sole discretion, (i) whether the Vulnerability needs correction and (ii) whether to apply any correction reported by the Researcher. 
The Researcher accepts to provide support to the Owners to verify the potential Vulnerability, and the Owners will update the researcher on any corrections applied.

9.	RESPONSIBLE DISCLOSURE

The Program follows a “responsible disclosure” policy. The following rules apply cumulatively:
a)	No Vulnerability shall be published without the Researcher having followed previously the Reporting Procedure set out in Clause 8 above;
b)	No Vulnerability shall be published without the Researcher having at least received the acknowledgment from the Owners on the reported Vulnerability.
c)	No Vulnerability shall be published within a period of forty five (45) days since the last communication exchanged with the Owners with regards to such potential Vulnerability, unless the Owners have agreed to a shorter period or defined a longer period.

10.	CODE OF CONDUCT

The GitLab Community Code of Conduct (https://about.gitlab.com/community/contribute/code-of-conduct/) applies with respect to the Agreement.

11.	DISCLAIMER

The EV Solution Source Code is provided “as is”. 
The Owners hereby disclaim any support, representation or warranty of any kind whatsoever, including any implied warranty of fitness for a particular purpose (including the compilation or execution of the source code) or non-infringement.

12.	LIABILITY DISCLAIMER

In no event will the Owners be liable for any damages whatsoever including – but not restricted to – lost revenue or profits or other direct, indirect, special, incidental, cover, or consequential damages arising out of the access to or inability to access the Electronic Voting Solution Source Code, even if advised of the possibility of such damages, except to the extent provided by law (where applicable). 

13.	TERMINATION

The Agreement and Source Code Access expires at the termination of the productive use of the release to which the Program is dedicated. 
The right to access the EV Solution shall terminate immediately and automatically without notice if the Researcher fails to comply with the terms and conditions under this Agreement. The Researcher shall destroy any copies of the EV Solution Source Code upon termination. 
The expiry or termination of the Agreement shall not affect the validity of the obligations of the Researcher entered into under the Agreement (including but not limited to the Fair Use Restrictions, the Reporting Procedure and the Responsible Disclosure).

14.	ENTIRE AGREEMENT

This Agreement constitutes the entire agreement between the Owners and the Researcher enabling access to the EV Solution Source Code.
Researchers will be allowed to access the EV Solution Source Code only if they accept the Agreement by registering for the Program as defined above.

15.	APPLICABLE LAW AND JURISDICTION

This Agreement is governed by and construed in accordance with the laws of Switzerland. 
Any dispute arising out of or relating to the Agreement shall be submitted and finally resolved by the courts of Berne, Switzerland.
